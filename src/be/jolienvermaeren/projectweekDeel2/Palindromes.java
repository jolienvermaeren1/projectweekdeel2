package be.jolienvermaeren.projectweekDeel2;

import java.io.*;
import java.util.*;

public class Palindromes {
    public static void CheckIfPalindrome() {
        Scanner in = new Scanner(System.in);
        String origString = in.nextLine();
        int length = origString.length();
        boolean isPalindrome = true;
        for (int beginIndex = 0; beginIndex < length; beginIndex++) {
            if (origString.charAt(beginIndex) != origString.charAt(length - 1 - beginIndex)) {
                System.out.println(origString + " is not a palindrome.");
                isPalindrome = false;
                break;
            }
        }if (isPalindrome) {
            System.out.println(origString + " is a palindrome.");
        }
    }

    public static void GiveNextPalindrome() {
        Scanner keyboard = new Scanner(System.in);
        int num = keyboard.nextInt();
        String numString = String.valueOf(num);
        int l = numString.length();
        int n = 2;
        int nextNumber = num + 1;
        if (l >= 2 && l <= 15) {
            while (n != 0) {
                int tempNextNumber = nextNumber;
                int reversed = 0;
                while (tempNextNumber != 0) {
                    int digit = tempNextNumber % 10;
                    reversed = reversed * 10 + digit;
                    tempNextNumber /= 10;
                }
                if (reversed == nextNumber) {
                    System.out.print(reversed);
                    break;
                }
                n++;
                nextNumber++;

            }
        }
    }

    public static void makePalindrom(){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a String: ");
        String givenText = keyboard.nextLine();

        StringBuilder givenText1 = new StringBuilder();

        //append a string into StringBuilder givenText1
        givenText1.append(givenText);

        //reverse StringBuilder givenText1
        givenText1 = givenText1.reverse();

        //print String with the reversed String
        System.out.println("The palindrome you've created is " + givenText + givenText1);


    }
}
