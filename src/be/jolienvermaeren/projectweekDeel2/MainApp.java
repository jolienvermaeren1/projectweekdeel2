package be.jolienvermaeren.projectweekDeel2;
import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        for (int i = 0; i >= 0; i++) {
            Menu.ShowMenu(); //shows the menu
            int choice = keyboard.nextInt();
            if (choice == 1) { //if choice is 1, print a Fibonacci sequence
                FibonacciNumbers.printFibonacciSequence();
                System.out.println("\n");
                continue;
            } else if (choice == 2) {
                System.out.print("Enter a limit: ");
                int limit = keyboard.nextInt();
                System.out.println(FibonacciNumbers.evenFibSum(limit));
                System.out.println("\n");
                continue;
            } else if (choice == 3) {
                System.out.println("Enter a number: ");
                int inputNumber = keyboard.nextInt();
                boolean isItPrime = PrimeNumbers.checkifPrimeNumber(inputNumber);
                if (isItPrime) {
                    System.out.println(inputNumber + " is a prime number.");
                } else {
                    System.out.println(inputNumber + " is not a prime number.");
                }
                System.out.println("\n");
                continue;
            } else if (choice == 4) {
                System.out.println("Enter a number: ");
                int inputNumber = keyboard.nextInt();
                System.out.println("The next prime number after " + inputNumber + " is " + PrimeNumbers.PrintNextPrimeNumber(inputNumber));
                System.out.println("\n");
                continue;
            } else if (choice == 5) {
                System.out.print("Enter a starting number: ");
                int startNumber = keyboard.nextInt();
                System.out.print("Enter an ending number: ");
                int endNumber = keyboard.nextInt();
                for (int j = startNumber; startNumber <= endNumber; ++startNumber) {
                    i++;
                    if (PrimeNumbers.isPrime(j) && j < endNumber) {
                        System.out.println("first prime number between " + startNumber + " and " + endNumber + " is " + j);
                        System.out.println("\n");
                        break;
                    } else if (!PrimeNumbers.isPrime(j)) {
                        break;
                    } else {
                        System.out.println("0");
                        System.out.println("\n");
                        break;
                    }
                }
                continue;
            } else if (choice == 6) {
                System.out.print("Enter a string: ");
                Palindromes.CheckIfPalindrome();
                System.out.println("\n");
                continue;
            } else if (choice == 7) {
                System.out.print("Enter a number: ");
                Palindromes.CheckIfPalindrome();
                System.out.println("\n");
                continue;
            } else if (choice == 8) {
                System.out.print("Enter a number: ");
                int num = keyboard.nextInt();
                Palindromes.GiveNextPalindrome();
                System.out.println("\n");
                continue;
            }else if (choice == 10) {
                Palindromes.makePalindrom();
            }
        }
    }
}
