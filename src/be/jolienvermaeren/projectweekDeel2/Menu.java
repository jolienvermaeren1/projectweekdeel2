package be.jolienvermaeren.projectweekDeel2;

public class Menu {
    public static void ShowMenu(){
        System.out.println("What would you like to do?\n(1) Print a Fibonacci sequence up to a given number\n" +
                "(2) Print the sum of all even Fibonacci numbers smaller than a given limit \n" +
                "(3) Check if a number is a prime number\n" +
                "(4) Print the next prime number after a given number \n" +
                "(5) Print a the first prime number between two given intervals \n" +
                "(6) Check is a String is a palindrome \n" +
                "(7) Check if a number is a palindrome \n" +
                "(8) Print the next palindrome after a given number \n" +
                "(10) Create a palindrome");
    }
}
