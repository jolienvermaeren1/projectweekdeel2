package be.jolienvermaeren.projectweekDeel2;
import java.util.Scanner;

public class FibonacciNumbers {


    static void printFibonacciSequence() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a maximum value: ");
        int maximumValue = keyboard.nextInt();
        int startValue = 1; //this is the first Fibonacci number in the sequence
        int nextFibonacciNumber = 1; //this is the next Fibonnacci number in the sequence
        System.out.println("All Fibonacci numbers up to " + maximumValue + ": ");
        while (startValue <= maximumValue) {
            System.out.print(startValue + ", "); //prints the start value
            int sumOf2 = startValue + nextFibonacciNumber; //calculates the sum of the last 2 Fibonacci numbers
            startValue = nextFibonacciNumber; //start value becomes the next Fibonacci number
            nextFibonacciNumber = sumOf2; // nextFibonacci becomes the sum of the last 2 Fibonacci numbers
        }
    }

    static int evenFibSum(int limit)
    {
        if (limit < 2) {
            return 0;
        }

        // Initialize first two even Fibonacci numbers and their sum
        long firstFibonacciNumber = 0;
        long secondFibonacciNumber = 2;
        long sum = firstFibonacciNumber+ secondFibonacciNumber;

        // calculating sum of even Fibonacci value
        while (secondFibonacciNumber <= limit) {
            // get next even value of Fibonacci sequence
            long thirdFibonacciNumber = 4 * secondFibonacciNumber + firstFibonacciNumber;
            // If we go beyond limit, we break loop
            if (thirdFibonacciNumber > limit) {
                break;
            }
            // Move to next even number and update sum
            firstFibonacciNumber = secondFibonacciNumber;
            secondFibonacciNumber = thirdFibonacciNumber;
            sum += secondFibonacciNumber;
        }
        System.out.print("The sum of all even Fibonacci numbers up to " + limit + " is ");
        return(int) sum;
    }
}
