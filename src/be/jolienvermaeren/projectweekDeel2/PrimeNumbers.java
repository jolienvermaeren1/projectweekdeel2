package be.jolienvermaeren.projectweekDeel2;

public class PrimeNumbers {

    static boolean checkifPrimeNumber(int inputNumber) {
        boolean isItPrime = true;
        if (inputNumber <= 1) {
            isItPrime = false;
            return isItPrime;
        } else {
            for (int i = 2; i <= inputNumber / 2; i++) {
                if ((inputNumber % i) == 0) {
                    isItPrime = false;
                    break;
                }
            }
            return isItPrime;
        }
    }

    static int PrintNextPrimeNumber(int inputNumber) {
        // Base case
        if (inputNumber <= 1) {
            return 2;
        }
        int prime = inputNumber;
        boolean found = false;
        // Loop continuously until CheckIfPrimeNumber returns
        // true for a number greater than n
        while (!found) {
            prime++;
            if (checkifPrimeNumber(prime)) {
                found = true;
            }
        }
        return prime;
    }

    //first prime number between two intervals
    public static boolean isPrime(int n)
    {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

}



